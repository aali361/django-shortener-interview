import string
from random import randrange
from random import choice
from locust import HttpUser, task, between

short_urls = []
class WebsiteUser(HttpUser):
    wait_time = between(0,0)

    def on_start(self):
        username = f'test{randrange(1000000000)}'
        response = self.client.post('/v1/user/sign-up/', {'email': f'{username}@aut.ac.ir', 'username':username, 'password': username})
        self.token = response.json()['token']
        self.url = None

    @task(1)
    def shorten(self):
        url = ''.join(choice(string.ascii_letters + string.digits) for x in range(12))
        response = self.client.post(
            '/v1/shortener/shorten/', 
            {'url':f'http://{url}.com'},
            headers={'Authorization': f'Token {self.token}'}
        )
        short_urls.append(response.json()['short'].split('/')[-2])
    
    @task(17)
    def get_url(self):
        self.client.get(f'/v1/shortener/r/{choice(short_urls)}', headers={'Authorization': f'Token {self.token}'})

    @task(2)
    def get_reports(self):
        response = self.client.get(f'/v1/shortener/reports/', headers={'Authorization': f'Token {self.token}'})
        report_id = choice(response.json()['results'])['id']
        self.client.get(f'/v1/today_report/{report_id}/', headers={'Authorization': f'Token {self.token}'})
